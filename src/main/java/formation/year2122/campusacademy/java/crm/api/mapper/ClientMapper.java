package formation.year2122.campusacademy.java.crm.api.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import ch.qos.logback.core.net.server.Client;
import formation.year2122.campusacademy.java.crm.api.model.ClientDTO;

@Mapper(componentModel = "spring")
public abstract class ClientMapper {

    @Mapping(target = "clientName", source = "clientName")
    public abstract ClientDTO map(Client client);

    abstract List<ClientDTO> map(List<Client> clients);

}
