package formation.year2122.campusacademy.java.crm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import formation.year2122.campusacademy.java.crm.api.model.ClientMinimalDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "\"order\"")
@Data
@NoArgsConstructor
public class Order extends ClientMinimalDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // long

    @Column(name = "tjmHt", nullable = false)
    private Float tjmHt;

    @Column(name = "nbJours", nullable = false)
    private Long nbJours;

    @Column(name = "tva", nullable = false)
    private Float tva;

    @Column(name = "state", nullable = false, length = 100)
    private String state;

    @Column(name = "type_prestation", nullable = false)
    private String typePresta;

    @Column(name = "comment", nullable = false)
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private Client client;

}
