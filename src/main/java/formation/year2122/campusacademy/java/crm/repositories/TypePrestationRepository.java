package formation.year2122.campusacademy.java.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import formation.year2122.campusacademy.java.crm.model.TypePrestation;

@RepositoryRestResource(exported = true)
public interface TypePrestationRepository extends JpaRepository<TypePrestation, String> {

}
