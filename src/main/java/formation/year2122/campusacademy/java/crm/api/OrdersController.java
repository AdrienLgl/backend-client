package formation.year2122.campusacademy.java.crm.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import formation.year2122.campusacademy.java.crm.api.model.OrderDTO;
import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.services.ClientService;
import formation.year2122.campusacademy.java.crm.services.OrderService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import java.util.stream.Collectors;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "orders")
public class OrdersController {

    private final OrderService orderService;
    private final ClientService clientService;

    @GetMapping
    public ResponseEntity<List<OrderDTO>> getAll() {
        return ResponseEntity.ok(this.orderService.findAll().stream().map(this::map).collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<OrderDTO> getById(@PathVariable Long id) {
        return this.orderService.findById(id).map(order -> ResponseEntity.ok(map(order)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> create(@RequestBody OrderDTO orderDTO) {
        Order order = new Order();
        Optional<Client> client = this.clientService.findById(orderDTO.getClientId());
        if (client.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        order.setClient(client.get());
        order.setComment(orderDTO.getComment());
        order.setNbJours(orderDTO.getNbJours());
        order.setTjmHt(orderDTO.getTjmHt());
        order.setTva(orderDTO.getTva());
        order.setTypePresta(orderDTO.getTypePresta());
        order.setState(orderDTO.getState());
        return ResponseEntity.ok(map(this.orderService.create(order)));
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> update(@PathVariable Long id, @RequestBody OrderDTO orderDTO) {
        Optional<Order> orderOptional = this.orderService.findById(id);
        if (orderOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Optional<Client> client = this.clientService.findById(orderDTO.getClientId());
        if (client.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Order orderToUpdate = orderOptional.get();
        orderToUpdate.setClient(client.get());
        orderToUpdate.setComment(orderDTO.getComment());
        orderToUpdate.setNbJours(orderDTO.getNbJours());
        orderToUpdate.setState(orderDTO.getState());
        orderToUpdate.setTjmHt(orderDTO.getTjmHt());
        orderToUpdate.setTva(orderDTO.getTva());
        orderToUpdate.setTypePresta(orderDTO.getTypePresta());
        return ResponseEntity.ok(map(this.orderService.update(orderToUpdate)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.orderService.delete(id);
    }

    private OrderDTO map(@NonNull Order order) {
        return new OrderDTO(order.getId(), order.getTjmHt(), order.getNbJours(), order.getTva(), order.getTypePresta(),
                order.getClient().getId(), order.getComment(), order.getState());
    }

}
