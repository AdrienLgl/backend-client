package formation.year2122.campusacademy.java.crm.api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderDTO extends OrderMinimalDTO {
    Long id;
    Long clientId;

    public OrderDTO(Long id, Float tjmHt, Long nbJours, Float tva, String typePresta, Long clientId, String comment,
            String state) {
        super(typePresta, tjmHt, nbJours, tva, comment, state);
        this.clientId = clientId;
        this.id = id;
    }
}