package formation.year2122.campusacademy.java.crm.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.repositories.ClientRepository;

@Component
public class ClientService {

    private ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> findAll() {
        return this.clientRepository.findAll();
    }

    public Optional<Client> findById(Long id) {
        return this.clientRepository.findById(id);
    }

    public Client create(Client client) {
        client.setId(null); // case of id already exists
        return this.clientRepository.save(client);
    }

    public Client update(Client client) {
        return this.clientRepository.save(client);
    }

    public void delete(Long id) {
        this.clientRepository.deleteById(id);
    }

    public List<Client> findByName(String clientName, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.clientRepository.findClientByClientName(clientName, pageable);
    }
}
