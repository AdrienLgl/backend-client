package formation.year2122.campusacademy.java.crm.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import formation.year2122.campusacademy.java.crm.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
