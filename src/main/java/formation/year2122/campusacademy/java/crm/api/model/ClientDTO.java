package formation.year2122.campusacademy.java.crm.api.model;

import java.util.List;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClientDTO extends ClientMinimalDTO {
    String state;
    Long ca;
    List<OrderMinimalDTO> orders;

    public ClientDTO(Long id, String name, Long ca, String state, List<OrderMinimalDTO> orders) {
        super(id, name);
        this.state = state;
        this.ca = ca;
        this.orders = orders;
    }
}
