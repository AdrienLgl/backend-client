package formation.year2122.campusacademy.java.crm.api.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderMinimalDTO {
    String typePresta;
    Float tjmHt;
    Long nbJours;
    Float tva;
    String comment;
    String state;
}