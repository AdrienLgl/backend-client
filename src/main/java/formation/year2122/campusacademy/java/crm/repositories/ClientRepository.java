package formation.year2122.campusacademy.java.crm.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import formation.year2122.campusacademy.java.crm.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

    List<Client> findClientByClientName(String clientName, Pageable pageable);

}
