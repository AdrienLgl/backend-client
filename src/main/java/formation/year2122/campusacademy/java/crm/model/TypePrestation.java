package formation.year2122.campusacademy.java.crm.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "type_prestation")
@Data
@NoArgsConstructor
public class TypePrestation {

    @Id
    private String name;
    private String description;

}
