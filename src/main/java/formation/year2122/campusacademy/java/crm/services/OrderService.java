package formation.year2122.campusacademy.java.crm.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.repositories.OrderRepository;

@Component
public class OrderService {

    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> findAll() {
        return this.orderRepository.findAll();
    }

    public Optional<Order> findById(Long id) {
        return this.orderRepository.findById(id);
    }

    public Order create(Order order) {
        order.setId(null);
        return this.orderRepository.save(order);
    }

    public Order update(Order order) {
        return this.orderRepository.save(order);
    }

    public void delete(Long id) {
        this.orderRepository.deleteById(id);
    }

}
