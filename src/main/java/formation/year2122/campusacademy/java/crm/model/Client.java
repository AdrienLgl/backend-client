package formation.year2122.campusacademy.java.crm.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "client")
@Data
@NoArgsConstructor
public class Client {

    public enum ClientState {
        ACTIF, INACTIF, ERROR
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // long

    @Column(name = "client_name", nullable = false, length = 100)
    private String clientName;

    @Column(name = "client_ca", nullable = false)
    private Long ca;

    @Column(name = "client_state")
    private String clientState;

    @OneToMany(mappedBy = "client", cascade = CascadeType.PERSIST)
    private List<Order> orders = new ArrayList<Order>();

}
