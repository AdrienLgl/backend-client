package formation.year2122.campusacademy.java.crm.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import formation.year2122.campusacademy.java.crm.api.model.ClientDTO;
import formation.year2122.campusacademy.java.crm.api.model.OrderMinimalDTO;
import formation.year2122.campusacademy.java.crm.model.Client;
import formation.year2122.campusacademy.java.crm.model.Order;
import formation.year2122.campusacademy.java.crm.services.ClientService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "clients")
public class ClientsController {

    private final ClientService clientService;

    @GetMapping
    public ResponseEntity<List<ClientDTO>> getAll() {
        return ResponseEntity.ok(this.clientService.findAll().stream().map(this::map).collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ClientDTO> getById(@PathVariable Long id) {
        return this.clientService.findById(id).map(client -> ResponseEntity.ok(map(client)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "search")
    public ResponseEntity<List<ClientDTO>> getByName(@RequestParam String name,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "10") int size) {
        return ResponseEntity.ok(
                this.clientService.findByName(name, page, size).stream().map(this::map).collect(Collectors.toList()));
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDTO> create(@RequestBody ClientDTO clientDTO) {
        Client client = new Client();
        client.setClientName(clientDTO.getName());
        client.setClientState(clientDTO.getState());
        client.setCa(clientDTO.getCa());
        return ResponseEntity.ok(map(this.clientService.create(client)));
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDTO> update(@PathVariable Long id, @RequestBody ClientDTO clientDTO) {
        Optional<Client> clientOptional = this.clientService.findById(id);
        if (clientOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        Client clientToUpdate = clientOptional.get();
        clientToUpdate.setClientName(clientDTO.getName());
        clientToUpdate.setCa(clientDTO.getCa());
        clientToUpdate.setClientState(clientDTO.getState());
        return ResponseEntity.ok(map(this.clientService.update(clientToUpdate)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.clientService.delete(id);
    }

    private ClientDTO map(@NonNull Client client) {
        List<OrderMinimalDTO> orders = map(client.getOrders());
        return new ClientDTO(client.getId(), client.getClientName(), client.getCa(), client.getClientState(), orders);
    }

    private List<OrderMinimalDTO> map(List<Order> orders) {
        List<OrderMinimalDTO> minimalOrders = new ArrayList<OrderMinimalDTO>();
        for (Order order : orders) {
            OrderMinimalDTO o = new OrderMinimalDTO(order.getTypePresta(), order.getTjmHt(), order.getNbJours(),
                    order.getTva(), order.getComment(), order.getState());
            minimalOrders.add(o);
        }
        return minimalOrders;
    }
}
